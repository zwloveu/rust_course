pub fn get_type_name_of<T>(_: T) -> &'static str {
    std::any::type_name::<T>()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_type_name_of() {
        let a = 1_i64;
        let b = &a;
        let ref c = a;

        assert_eq!(get_type_name_of(a), "i64");
        assert_eq!(get_type_name_of(b), "&i64");
        assert_eq!(get_type_name_of(c), "&i64");
        assert_eq!(b, c);
        let ref d = &5;
        assert_eq!(get_type_name_of(d), "&&i32");
        let e = **d;
        assert_eq!(5, e);
    }
}
