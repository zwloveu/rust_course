use std::io::{BufRead, Write};

pub struct Quizzer<R, W> {
    pub reader: R,
    pub writer: W,
}

impl<R, W> Quizzer<R, W>
where
    R: BufRead,
    W: Write,
{
    pub fn prompt(&mut self, question: &str) -> String {
        if !question.is_empty() {
            write!(&mut self.writer, "{}", question).expect("unable to write");
        }

        let mut s = String::new();
        self.reader.read_line(&mut s).expect("unable to read");

        s
    }
}

/// Return u32 number by inputting a string in a console
///
/// # Examples
///
/// ```
/// extern crate utils;
/// use utils::input::get_input_number;
/// let result = get_input_number("1234");
/// ```
pub fn get_input_number(guess: &str) -> Option<u32> {
    match guess.trim().parse() {
        Ok(num) => Some(num),
        Err(_) => None,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_input_number() {
        let input = b"1234";
        let mut output = Vec::new();

        let input_reader_string = {
            let mut quizzer = Quizzer {
                reader: &input[..],
                writer: &mut output,
            };

            quizzer.prompt("Please input your number\r\n")
        };

        let output = String::from_utf8(output).expect("Not UTF-8");
        assert_eq!(output, "Please input your number\r\n");

        let guess_number = get_input_number(input_reader_string.as_str());
        assert_eq!(guess_number.unwrap(), 1234);
    }
}
