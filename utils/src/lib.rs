mod rand_number;
pub use rand_number::generate_rand_number;

pub mod input;

pub mod types;
