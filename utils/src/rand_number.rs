use rand::distributions::uniform::{SampleRange, SampleUniform};
use rand::Rng;

/// Generate a random value in the given range.
///
/// Only `generate_rand_number(low..high)` and `generate_rand_number(low..=high)` are supported.
///
/// # Panics
///
/// Panics if the range is empty.
///
/// # Example
///
/// ```
/// extern crate utils;
///
/// // Exclusive range
/// let n: u32 = utils::generate_rand_number(0..10);
/// println!("{}", n);
/// let m: f64 = utils::generate_rand_number(-40.0..1.3e5);
/// println!("{}", m);
///
/// // Inclusive range
/// let n: u32 = utils::generate_rand_number(0..=10);
/// println!("{}", n);
/// ```
///
pub fn generate_rand_number<T, R>(range: R) -> T
where
    T: SampleUniform,
    R: SampleRange<T>,
{
    let _a = rand::thread_rng().gen_range(1..101);
    rand::thread_rng().gen_range(range)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_generate_rand_number() {
        let result = generate_rand_number(100..101);
        assert_eq!(true, result == 100);

        let result1 = generate_rand_number(101..=101);
        assert_eq!(true, result1 == 101);
    }
}
