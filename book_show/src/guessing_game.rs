use std::cmp::Ordering;
use std::io::{stdin, stdout};
use utils::generate_rand_number;
use utils::input::{get_input_number, Quizzer};

pub fn guess() {
    println!("Guess the number!");

    let stdio = stdin();
    let input = stdio.lock();
    let output = stdout();

    let secret_number = generate_rand_number(1..101);

    let mut quizzer = Quizzer {
        reader: input,
        writer: output,
    };

    loop {
        let guess_string = quizzer.prompt("Please input your number:\r\n");
        let guess_num = match get_input_number(guess_string.as_str()) {
            Some(num) => Some(num),
            None => continue,
        };

        let guess = guess_num.unwrap();
        println!("You guessed: {}", guess);

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}
