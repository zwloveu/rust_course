mod guessing_game;
mod hello_world;

use language_feature::{
    data_interact, rum_patterns_and_match, run_arrays, run_enums, run_error_handling,
};

pub use language_feature::create_user;

pub async fn run_language_features() {
    data_interact();
    create_user();
    run_enums();
    rum_patterns_and_match();
    run_arrays();
    run_error_handling().await;
}

pub fn run_local_features() {
    hello_world::print_hello_world();
    guessing_game::guess();
}
