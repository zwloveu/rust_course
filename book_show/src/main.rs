#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    book_show::run_local_features();
    book_show::run_language_features().await;
    book_show::create_user();

    Ok(())
}
