/// We use the owned *String* type rather then the &str *String* slice type.
/// This is a delibrate choice because we want instances of this struct
/// to own all of its data and for that data to be valid
/// as long as the entire struct is valid
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

struct UserByLifeTime<'a> {
    username: &'a str,
    email: &'a str,
    sign_in_count: u64,
    active: bool,
}

pub fn create_user() {
    let user1 = User {
        email: String::from("another@example.com"),
        username: String::from("anotherusername567"),
        active: true,
        sign_in_count: 1,
    };
    let user1_by_lifetime = UserByLifeTime {
        email: "someone@example.com",
        username: "someusername123",
        active: true,
        sign_in_count: 1,
    };

    println!(
        "user1:username={},email={},sign_in_count={},active={}",
        user1.username, user1.email, user1.sign_in_count, user1.active
    );
    println!(
        "user1_by_lifetime:username={},email={},sign_in_count={},active={}",
        user1_by_lifetime.username,
        user1_by_lifetime.email,
        user1_by_lifetime.sign_in_count,
        user1_by_lifetime.active
    );

    let user2 = build_user(user1.email, user1.username);
    println!(
        "user2:username={},email={},sign_in_count={},active={}",
        user2.username, user2.email, user2.sign_in_count, user2.active
    );

    let user2_by_lifetime =
        build_user_by_lifetime(user1_by_lifetime.email, user1_by_lifetime.username);
    println!(
        "user2_by_lifetime:username={},email={},sign_in_count={},active={}",
        user2_by_lifetime.username,
        user2_by_lifetime.email,
        user2_by_lifetime.sign_in_count,
        user2_by_lifetime.active
    );
}

fn build_user(email: String, username: String) -> User {
    User {
        email,
        username,
        active: true,
        sign_in_count: 1,
    }
}

fn build_user_by_lifetime<'a>(email: &'a str, username: &'a str) -> UserByLifeTime<'a> {
    UserByLifeTime {
        email,
        username,
        active: true,
        sign_in_count: 1,
    }
}
