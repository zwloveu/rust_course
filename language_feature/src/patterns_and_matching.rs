/// This shows all the places where patterns are valid
///
pub fn rum_patterns_and_match() {
    match_arms();
    conditional_if_let();
    conditional_while_let();
    for_loops();
    funtion_parameters();
    bingdings();
}

/// Formally, *match* expressions are defined as the key word *match*,
/// a value to match on, and one or more match arms that consist of
/// a pattern and an expression to run if the values
/// matches that arm's pattern
///
///
/// One requirement for *match* is that they need to be exhaustive in the sense that all
/// possibilities for the value in the *match* expression must be accounted for.
///
/// The "_" pattern can be useful when you want to ingore any value not specified.
fn match_arms() {
    let x = 9;
    let message = match x {
        0 | 1 => "not many",
        2..=9 => "a few",
        _ => "lots",
    }; // here message = "a few"
    println!("x = 9 means {}", message);
}

fn conditional_if_let() {
    let favourite_color: Option<&str> = None;
    let is_tuesday = false;
    let age: Result<u8, _> = "34".parse();

    if let Some(color) = favourite_color {
        println!("Using your favorite color, {}, as the background", color);
    } else if is_tuesday {
        println!("Tuesday is green day!");
    } else if let Ok(temp_age) = age {
        if temp_age > 30 {
            println!("Using purple as the background color");
        } else {
            println!("Using orange as the background color");
        }
    } else {
        println!("Using blue as the background color");
    }
}

fn conditional_while_let() {
    let mut stack = Vec::new();

    stack.push(1);
    stack.push(2);
    stack.push(3);
    stack.push(4);

    while let Some(top) = stack.pop() {
        println!("{}", top);
    }
}

fn for_loops() {
    let v = vec![1, 2, 3, 4];

    for (index, value) in v.iter().enumerate() {
        println!("{} is at index {}", value, index);
    }
}

fn funtion_parameters() {
    let point = (3, 5);
    print_coordinates(&point);
    fn print_coordinates(&(x, y): &(i32, i32)) {
        println!("Current location: ({}, {})", x, y);
    }
}

/// The "@"(at) operator lets us create a variable that holds a value
/// at the same time we're testing that value to see whether
/// it matches a pattern
fn bingdings() {
    enum Message {
        Hello { id: i32 },
    }

    let msg = Message::Hello { id: 5 };

    match msg {
        Message::Hello { id: id_var @ 3..=7 } => println!("Found an id in range: {}", id_var),
        Message::Hello { id: 10..=12 } => println!("Found an id in another range"),
        Message::Hello { id } => println!("Found some other id: {}", id),
    }
}
