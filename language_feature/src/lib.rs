pub mod traits;

mod data_interact;
pub use data_interact::data_interact;

mod structs;
pub use structs::create_user;

mod enums;
pub use enums::run_enums;

mod patterns_and_matching;
pub use patterns_and_matching::rum_patterns_and_match;

mod arrays;
pub use arrays::run_arrays;

mod error_handling;
pub use error_handling::run_error_handling;
