enum WebEvent {
    PageLoad,
    PageUnload,
    KeyPress(char),
    Paste(String),
    Click { x: i64, y: i64 },
}

fn inspect(event: &WebEvent) {
    match event {
        WebEvent::PageLoad => println!("page loaded"),
        WebEvent::PageUnload => println!("page unloaded"),
        WebEvent::KeyPress(c) => println!("pressed '{}'", c),
        WebEvent::Paste(s) => println!("pasted \"{}\"", s),
        WebEvent::Click { x, y } => println!("clicked at x={}, y={}", x, y),
    }
}

fn perform_webevents() {
    let pressed = WebEvent::KeyPress('x');
    // `to_owned()` creates an owned `String` from a string slice.
    let pasted = WebEvent::Paste("my text".to_owned());
    let click = WebEvent::Click { x: 20, y: 80 };
    let load = WebEvent::PageLoad;
    let unload = WebEvent::PageUnload;

    inspect(&pressed);
    inspect(&pasted);
    inspect(&click);
    inspect(&load);
    inspect(&unload);
}

enum VeryVerboseEnumOfThingsToDoWithNumbers {
    Add,
    Substract,
}

impl VeryVerboseEnumOfThingsToDoWithNumbers {
    fn run(&self, x: i64, y: i64) -> i64 {
        match self {
            Self::Add => x + y,
            Self::Substract => x - y,
        }
    }
}

fn perfom_very_verbose() {
    type Operations = VeryVerboseEnumOfThingsToDoWithNumbers;
    let add = Operations::Add;
    let add_result = add.run(54, 49);
    println!("54 + 49 = {}", add_result);
    println!("54 - 49 = {}", Operations::Substract.run(54, 49));
    println!("VeryVerboseEnumOfThingsToDoWithNumbers::Add={}", add as i32);
}

#[derive(Debug)]
enum MyList {
    Cons(u32, Box<MyList>),
    Nil,
}

impl MyList {
    fn new() -> MyList {
        MyList::Nil
    }

    fn prepend(self, element: u32) -> MyList {
        MyList::Cons(element, Box::new(self))
    }

    fn len(&self) -> u32 {
        match self {
            MyList::Cons(_, ref tail) => 1 + tail.len(),
            MyList::Nil => 0,
        }
    }

    fn stringify(&self) -> String {
        match self {
            MyList::Cons(head, ref tail) => {
                println!("head={}", head);
                format!("{},{}", head, tail.stringify())
            }
            MyList::Nil => format!("Nil"),
        }
    }
}

fn perform_my_list_operation() {
    // Create an empty linked list
    let mut list = MyList::new();
    // Prepend some elements
    list = list.prepend(1);
    list = list.prepend(2);
    list = list.prepend(3);
    // Show the final state of the list
    println!("linked list has length: {}", list.len());
    println!("{}", list.stringify());

    let my_list = MyList::Cons(
        1,
        Box::new(MyList::Cons(
            2,
            Box::new(MyList::Cons(3, Box::new(MyList::Nil))),
        )),
    );
    println!("{:?}", my_list);
    println!("{}", my_list.stringify());
}

fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        None => None,
        Some(i) => Some(i + 1),
    }
}

fn perform_plus_one() {
    let five = Some(5);
    let six = plus_one(five);
    let none = plus_one(None);
    // here, newly added in 1.58: Captured identifiers in format strings
    // https://blog.rust-lang.org/2022/01/13/Rust-1.58.0.html#captured-identifiers-in-format-strings
    println!("{:?},{six:?},{none:#?}", five);
}

#[derive(Debug)]
enum UsState {
    Alabama,
    Alaska,
}

enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}

fn value_in_cents(coin: Coin) -> u8 {
    match coin {
        Coin::Penny => {
            println!("Lucky penny!");
            1
        }
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter(state) => {
            println!("State quarter from {:?}!", state);
            25
        }
    }
}

fn perform_values_in_actions() {
    let mut value_cents = value_in_cents(Coin::Penny);
    println!("value for Coin::Penny = {value_cents}");
    value_cents = value_in_cents(Coin::Quarter(UsState::Alabama));
    println!("value for Coin::Quarter(UsState::Alabama) = {value_cents}");
    value_cents = value_in_cents(Coin::Quarter(UsState::Alaska));
    println!("value for Coin::Quarter(UsState::Alaska) = {value_cents}");
    value_cents = value_in_cents(Coin::Nickel);
    println!("value for Coin::Nickel = {value_cents}");
    value_cents = value_in_cents(Coin::Dime);
    println!("value for Coin::Dime = {value_cents}");
}

enum Action {
    Say(String),
    MoveTo(i32, i32),
    ChangeColorRGB(u16, u16, u16),
}

fn perform_actions() {
    let actions = [
        Action::Say("Hello Rust".to_string()),
        Action::MoveTo(1, 2),
        Action::ChangeColorRGB(255, 255, 0),
    ];

    for action in actions.iter() {
        match action {
            Action::Say(text) => println!("Say: {}", text),
            Action::MoveTo(x, y) => println!("Move to: ({}, {})", x, y),
            Action::ChangeColorRGB(r, g, b) => println!("Change color to: ({}, {}, {})", r, g, b),
        }
    }
}

pub fn run_enums() {
    perform_webevents();

    perfom_very_verbose();

    perform_my_list_operation();

    perform_plus_one();

    perform_values_in_actions();

    perform_actions();
}
