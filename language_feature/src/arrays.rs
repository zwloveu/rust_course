use std::io::stdin;

fn read_from_console() {
    let a = [1, 2, 3, 4, 5];
    println!("Please enter an array index.");

    let mut input = String::new();
    stdin().read_line(&mut input).expect("Failed to read line");
    println!("You typed: {}", input);

    let index: usize = input
        .trim()
        .parse()
        .expect("Index entered was not a number");
    let element = a[index];
    println!("The value of the element at index {index} is: {element}");
}

fn twod_array_using() {
    let one = [1, 2, 3];
    let two: [u8; 3] = [1, 2, 3];
    let blank1 = [0; 3];
    let blank2: [u8; 3] = [0; 3];
    //let slice1=&blank1[..];

    let arrays: [[u8; 3]; 4] = [one, two, blank1, blank2];

    // for a in &arrays, here, we use reference to avoid tranfering ownership
    // if we use "for a in arrays", the ownership will be transferred and will be no longer to use the arrays
    // for a in &arrays = for a in arrays.iter(), they are all immutable borrows
    // for a in &mut arrays = for a in arrays.iter_mut(), they are all mutable borrows
    for a in arrays.iter() {
        print!("{a:?}");

        for n in a.iter() {
            print!("\t{} + 10 = {}", n, n + 10);
        }

        let mut sum = 0;
        for i in 0..a.len() {
            sum += a[i];
        }

        println!("\t({:?} = {})", a, sum);
    }
}

pub fn run_arrays() {
    read_from_console();
    twod_array_using();
}
