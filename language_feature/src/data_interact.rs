pub fn data_interact() {
    stack_only_data_copy();
    stack_heap_data_move();
    stack_head_data_clone();
    stack_heap_data_refer_borrow();
}

/// This function shows what is copy in rust
///
/// Copy is used for type with data on stack
///
/// All the integer types, such as u32.
///
/// The Boolean type, bool, with values true and false.
///
/// All the floating point types, such as f64.
///
/// The character type, char.
///
/// Tuples, if they only contain types that also implement Copy.
///
/// For example, (i32, i32) implements Copy,
/// but (i32, String) does not.
fn stack_only_data_copy() {
    let x = 5; //bind the value 5 to x
    let y = x; //make a copy of the value in x and bind it to y
    println!("x={:?},y={:?}", x, y);
}

fn stack_heap_data_move() {
    let s1 = String::from("hello");
    let s1_heap_addr = s1.as_ptr();
    // here s1 is moved into s2, and s1 is no longer valid
    // copy the pointer, the length and the capacity that are on the stack
    // Do NOT copy the data on the heap that the pointer refers to.
    let s2 = s1;
    let s2_heap_addr = s2.as_ptr();

    println!(
        "
        s1_heap_addr={:p}, 
        s2={:?}, s2_addr={:p}",
        s1_heap_addr, s2, s2_heap_addr
    );
    //println!("s1={:?}",s1); s1 is no longer valid here
}

fn stack_head_data_clone() {
    let s1 = String::from("hello");
    let s1_heap_addr = s1.as_ptr();
    // here s2 is deeply copied from s1,
    // not only the stack data but also the heap data
    // copy the pointer, the length and the capacity that are on the stack
    // copy the data on the heap that the pointer refers to.
    let s2 = s1.clone();
    let s2_heap_addr = s2.as_ptr();

    // the heap content addresses of s1 and s2 are different
    println!(
        "
        s1_heap_addr={:p}, 
        s2={:?}, s2_addr={:p}",
        s1_heap_addr, s2, s2_heap_addr
    );
    println!("s1={:?}", s1);

    let s3: &str = "fsdfsdf";
    let s4 = s3;
    println!("{:p},{:p}", s3, s4);
}

fn stack_heap_data_refer_borrow() {
    let s1 = String::from("hello");
    // The &s1 syntax lets us create a reference that refers to the value of s1 but does not own it
    let len = calculate_length_by_borrow(&s1);
    println!("s1 after borrow is: {}", s1); // here s1 is valid
                                            // s1's value moves into the function and so is no longer valid afterward
    let len_of_slice = calculate_length_by_slice(&s1[..]);
    // we can not use the immutable reference after move in the next line calculate_length_by_ownership
    let (new_s1, len1) = calculate_length_by_ownership(s1);
    // println!("s1 is: {}", s1); here s1 is not valid, you can not use it.

    println!("s1 length = {}", len);
    println!("s1 length = {}", len1);
    println!("the length of slice of s1 is: {}", len_of_slice);
    println!("new s1 is: {}", new_s1);

    let mut s1: String = String::from("Hello");
    change(&mut s1);
    println!("mut s1 is: {}", s1);

    // if you want to use something after passing it to a function,
    // this is a Move action
    // 1. you can return it, just take ownership of the value
    // 2. you can pass in a reference to an object as a parameter

    /// the signature of the function uses & to indicate that the type of the parameter s is a reference
    fn calculate_length_by_borrow(s: &String) -> usize {
        s.len()
        // here, s goes out of scope.
        // But because it does not have ownership of what it refers to
        // nothing happens.
    }

    /// Slice is a data type that does not have ownership
    /// Slices let you reference a contiguous sequence of elements in a collection
    /// rather than the whole collection
    fn calculate_length_by_slice(s: &str) -> usize {
        s.len()
    }

    fn calculate_length_by_ownership(s: String) -> (String, usize) {
        let length = s.len();
        // this way here we return back the ownership of s
        (s, length)
        // here move this function's return value into the function that calls it
        // s and length are returned and move out to the calling function
    }

    fn change(s: &mut String) {
        s.push_str(" world");
    }

    let s1 = String::from("hello world");
    println!(
        "the first word of '{}' is '{}'",
        s1,
        &s1[0..first_word(&s1)]
    );
    println!(
        "the first word of '{}' is '{}'",
        s1,
        first_word_by_slice(&s1[..])
    );

    fn first_word(s: &String) -> usize {
        let bytes = s.as_bytes();
        for (i, &item) in bytes.iter().enumerate() {
            if item == b' ' {
                return i;
            }
        }
        s.len()
    }

    /// This is a more experienced way
    fn first_word_by_slice(s: &str) -> &str {
        let bytes = s.as_bytes();
        for (i, &item) in bytes.iter().enumerate() {
            if item == b' ' {
                return &s[0..i];
            }
        }
        &s[..]
    }
}
