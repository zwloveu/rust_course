use std::cmp::Ordering;

/// Eq and PartialEq are traits that allow you to define total and partial equality
/// between values, respectively.
///
/// Implementing them overloads the == and != operators.
///
/// Ord and PartialOrd are traits that allow you to define total and partial orderings
/// between values, respectively.
///
/// Implementing them overloads the <, <=, and >= operators.
///
#[derive(Eq)]
pub struct Person<'a> {
    pub id: u32,
    pub name: &'a str,
    pub height: u32,
}

impl<'a> Ord for Person<'a> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.height.cmp(&other.height)
    }
}

impl<'a> PartialOrd for Person<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<'a> PartialEq for Person<'a> {
    fn eq(&self, other: &Self) -> bool {
        self.height == other.height
    }
}

/// The From and Into traits are inherently linked,
/// and this is actually part of its implementation.
/// If you are able to convert type A from type B,
/// then it should be easy to believe that we should be able to convert type B to type A.
#[derive(Debug)]
pub struct Number {
    pub value: i32,
}

impl From<i32> for Number {
    fn from(value: i32) -> Self {
        Number { value }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_compare_person() {
        let p1 = Person {
            id: 1,
            name: "test1",
            height: 32,
        };

        let p2 = Person {
            id: 2,
            name: "test1",
            height: 33,
        };

        let p3 = Person {
            id: 3,
            name: "test1",
            height: 33,
        };
        assert_eq!(true, p1 < p2);
        assert_eq!(true, p2 == p3);
    }

    #[test]
    fn test_from_into() {
        let num = Number::from(5);
        assert_eq!(num.value, 5);
        let num: Number = 5.into();
        assert_eq!(num.value, 5);
    }
}
